terraform {
  backend "gcs" {
    bucket  = "github-bot-remote-state"
    prefix  = "terraform/github-bot-state"
    credentials = "github-bot.json"
  }
}

provider "google" {
  project = "github-bot-276123"
  region  = var.region
  zone    = var.zone
  credentials = file("github-bot.json")

  version = "~> 3.0"
}

resource "google_project_service" "run" {
  service = "run.googleapis.com"
}

resource "google_cloud_run_service" "githubbot" {
  name     = var.cloudrun_name
  location = var.region

  template {
    spec {
      containers {
        image = "gcr.io/github-bot-276123/github-bot:${var.job_id}"
        env {
          name = "CI_TAG"
          value = var.job_id
        }
        env {
          name = "GITHUB_KEY"
          value = var.github_key
        }
        env {
          name = "LOG_LEVEL"
          value = var.log_level
        }
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }

  autogenerate_revision_name = true
  depends_on = [google_project_service.run]
}

resource "google_cloud_run_service_iam_member" "allUsers" {
  service  = google_cloud_run_service.githubbot.name
  location = google_cloud_run_service.githubbot.location
  role     = "roles/run.invoker"
  member   = "allUsers"
}

