package utils

import "os"

//Helper function to obtain the value of an environment variable while passing in a default.
func Getenv(key string, defaultValue string) string {
	var toReturn string = os.Getenv(key)
	if len(toReturn) == 0 {
		return defaultValue
	}

	//The only other option
	return toReturn
}
