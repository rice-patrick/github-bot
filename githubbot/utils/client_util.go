package utils

import (
	github "github.com/google/go-github/v31/github"
	"log"
)

//Simple go class for maintaining a client access to the API
//The client itself
var client github.Client
//Which identity the client is used currently
var identity github.User

func SetIdentity(user github.User) {
	log.Printf("Identity for client set to: %s", *user.Login)
	identity = user
}
func GetIdentityLogin() *string {
	return identity.Login
}
func SetClient(newClient github.Client) {
	client = newClient
}
func GetClient() github.Client {
	return client
}