FROM scratch
# Note - badger is responsible in part of the creation of curl. I view this as a trusted source
# for pulling down certificates.
ADD https://raw.githubusercontent.com/bagder/ca-bundle/master/ca-bundle.crt /etc/ssl/certs/
ADD github-bot /

# This must be 8080 for CloudRun to be compatibe with the service
EXPOSE 8080
CMD ["/github-bot"]