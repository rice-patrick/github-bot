module gitlab.com/rice-patrick/github-bot

go 1.14

require (
	github.com/google/go-github/v31 v31.0.0
	github.com/stretchr/testify v1.5.1 // indirect
	github.com/tidwall/gjson v1.6.0
	golang.org/x/net v0.0.0-20190311183353-d8887717615a
	golang.org/x/oauth2 v0.0.0-20180821212333-d2e6202438be
)
