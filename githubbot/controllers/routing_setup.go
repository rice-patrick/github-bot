package controllers

import (
	"gitlab.com/rice-patrick/github-bot/event_handler"
	"net/http"
)

//Register a controller the accepts a list of events to monitor
func RegisterControllers(handler event_handler.EventHandler) {

	//Handle Events
	ec := newEventController(handler)
	http.Handle("/acceptEvent", ec)
	http.Handle("/acceptEvent/", ec)
}

//Register a controller that monitors all events
func RegisterDefaultControllers() {
	RegisterControllers(event_handler.NewEventHandler())
}