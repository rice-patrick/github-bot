variable "region" {
  description = "The region to create the resource in"
  default     = "us-central1"
}

variable "zone" {
  description = "The specific zone to create the resource in"
  default     = "us-central1-a"
}

variable "cloudrun_name" {
  description = "The name of the container to run within Cloud Run"
  default     = "github-bot"
}

variable "job_id" {
  description = "The Job ID that produced this revision. Needs to be distinct to guarantee a new revision is created"
}

variable "github_key" {
  description = "A Personal access token for the bot to use. Will be replaced with oauth eventually."
}

variable "log_level" {
  description = "The log level to use for debugging"
  default     = "info"
}