#!/bin/bash

# Taken with appreciation from https://gist.github.com/rahmonov/24dd2a326e52b1b9e56cb9018a65576a

IFS=$'\n\t'
set -eou pipefail

if [[ "$#" -ne 2 || "${1}" == '-h' || "${1}" == '--help' ]]; then
  cat >&2 <<"EOF"
gcrgc.sh cleans up tagged or untagged images pushed leaving only last specified number
for a given repository (an image name without a tag/digest).
USAGE:
  gcrgc.sh REPOSITORY AMOUNT-TO-LEAVE
EXAMPLE
  gcrgc.sh gcr.io/ahmet/my-app 3
  would clean up everything under the gcr.io/ahmet/my-app repository
  leaving only last 3.
EOF
  exit 1
fi

main(){
  local C=0
  IMAGE="${1}"
  AMOUNT_TO_LEAVE="${2}"
  INDEX=0
  for digest in $(gcloud container images list-tags ${IMAGE} --limit=999999 --sort-by=~TIMESTAMP --format='get(digest)'); do
    if [ $INDEX -ge $AMOUNT_TO_LEAVE ]; then
        (
          set -x
          gcloud container images delete -q --force-delete-tags "${IMAGE}@${digest}"
        )
        let C=C+1
    fi
    let INDEX=${INDEX}+1
  done
  echo "Deleted ${C} images in ${IMAGE}." >&2
}

main "${1}" "${2}"