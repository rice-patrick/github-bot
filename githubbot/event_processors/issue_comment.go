package event_processors

import (
	"fmt"
	"github.com/google/go-github/v31/github"
	"github.com/tidwall/gjson"
	"gitlab.com/rice-patrick/github-bot/utils"
	"golang.org/x/net/context"
	"log"
	"strconv"
)

//Processor for handling the issue comment events
type IssueCommentProcessor struct {

}

func (proc IssueCommentProcessor) CheckEvent(eventType string) bool {
	if eventType == "issue_comment"{
		return true
	} else {
		return false
	}
}

func (proc IssueCommentProcessor) ProcessEvent(event string) {
	log.Printf("Event received for issue comments")

	//Retrieve info needed to comment
	login := gjson.Get(event, "issue.user.login")
	repo := gjson.Get(event, "repository.name")
	issueId := gjson.Get(event, "issue.number")
	issueNum,_ := strconv.Atoi(issueId.String())

	if login.String() == *utils.GetIdentityLogin() {
		log.Printf("Comment received, but discarded due to using same login identity: %s. Responding would cause an infinite loop.", login)
	} else {
		proc.postResponse(login.String(), repo.String(), issueNum, login.String())
	}
}

func (proc IssueCommentProcessor) postResponse(owner string, repo string, issueNum int, poster string) {

	body := fmt.Sprintf("Hello back to %s", poster)
	comment := github.IssueComment{Body: &body}

	iComment, _, err := utils.GetClient().Issues.CreateComment(context.Background(), owner, repo, issueNum, &comment)
	if err != nil {
		log.Printf(err.Error())
	}

	if iComment != nil {
		log.Printf(fmt.Sprintf("URL For Comment: %s", iComment.URL))
	}
}
