package main

import (
	"fmt"
	"github.com/google/go-github/v31/github"
	"gitlab.com/rice-patrick/github-bot/controllers"
	"gitlab.com/rice-patrick/github-bot/event_handler"
	"gitlab.com/rice-patrick/github-bot/event_processors"
	utils "gitlab.com/rice-patrick/github-bot/utils"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"log"
	"net/http"
	"os"
	"strconv"
)

//main function
func main ()  {
	//Get the string value of the environment variable for the port, then convert it to base 10 integer.
	port, err := strconv.ParseInt(utils.Getenv("PORT", "8080"), 10 , 64)
	if err != nil {
		log.Fatalf("Invalid value provided for port, not a numeric value. %v provided.", os.Getenv("PORT"))
	}
	
	log.Print("Retrieving GitHub authenticated client")
	createAuthenticatedClient()

	log.Printf("Starting GitHub bot on port %d\n", port)
	startWebServer(port)
}

func createAuthenticatedClient() {

	ctx := context.Background()
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: utils.Getenv("GITHUB_KEY", "")},
	)
	tc := oauth2.NewClient(ctx, ts)

	client := github.NewClient(tc)

	user,_,_ := client.Users.Get(ctx, "") //using empty string will retrieve the current user

	utils.SetIdentity(*user)
	utils.SetClient(*client)
}


func startWebServer(port int64) {
	controllers.RegisterControllers(getHandledEvents())

	//Use the default
	http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}

func getHandledEvents() event_handler.EventHandler {
	handler := event_handler.NewEventHandler()
	//Remove comment processor, we only want PR processor for now.
	handler.RegisterProcessorForEvent(event_processors.PullRequestProcessor{})
	return handler
}

