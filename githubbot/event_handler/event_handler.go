package event_handler

import (
	"fmt"
	"log"
)

//An interface for creating event processors
type EventProcessor interface {
	CheckEvent(eventType string) bool
	ProcessEvent(event string)
}

type EventHandler struct {
	eventProcessors []EventProcessor
}

//Create a new event handler with an empty set of events
func NewEventHandler() EventHandler {
	var array []EventProcessor
	return EventHandler{
		eventProcessors: array,
	}
}

func (handler *EventHandler) RegisterProcessorForEvent(processor EventProcessor) {
	processors := append(handler.eventProcessors, processor)
	handler.eventProcessors = processors
}

func (handler *EventHandler) ProcessEvent(eventType string, eventBody string) {

	if len(handler.eventProcessors) == 0 {
		log.Printf("No event handlers are registered. Received event %s but skipping it.", eventType)
	} else {
		log.Printf(fmt.Sprintf("Received event for %s, checking processors.", eventType))
	}

	for p := range handler.eventProcessors {
		processor := handler.eventProcessors[p]
		if processor.CheckEvent(eventType) {
			processor.ProcessEvent(eventBody)
		}
	}
}