package controllers

import (
	"fmt"
	"gitlab.com/rice-patrick/github-bot/event_handler"
	"gitlab.com/rice-patrick/github-bot/utils"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"strings"
)

type eventController struct {
	handler event_handler.EventHandler
}

// Constructor function for creating the event controller
func newEventController(inputHandler event_handler.EventHandler) eventController {
	return eventController{
		handler: inputHandler, //yep, that trailing comma is required in go.
	}
}

//handle incoming http requests
//https://golang.org/pkg/net/http/#Handler
func (controller eventController) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	case "GET":
		http.Error(w, "GET not supported on this API", http.StatusBadRequest)
		return
	case "POST":
		controller.processEvent(w, r)
		return
	}
}

//Processes the input into a json interface, then prints the body for now.
//This will eventually be used to actually act upon the event
func (controller eventController) processEvent(w http.ResponseWriter, r *http.Request) (int, error) {

	//value used later for debugging
	dump,_ := httputil.DumpRequest(r, true)

	//Case insensitive compare
	if strings.EqualFold(utils.Getenv("LOG_LEVEL", "debug"), "debug") {
		log.Print("Debug Logging, event received")
		log.Print(string(dump))
	}


	//Get event type from the header
	eventType := r.Header.Get("X-GitHub-Event")
	if len(eventType) > 0 {
		log.Print(fmt.Sprintf("Received event from GitHub: %s ", eventType ))
	}

	//This is where the string will go
	var jsonString []byte

	contentType := r.Header.Get("Content-Type")
	if contentType == "application/x-www-form-urlencoded" {
		//Parse the form, then pull the data from "payload"
		r.ParseForm()
		jsonString = []byte(r.PostForm.Get("payload"))

	} else if contentType == "application/json" {
		//read the json straight from the body
		var err error
		jsonString, err = ioutil.ReadAll(r.Body)

		if err != nil {
			log.Print("Is your WebHook configured to send JSON correctly?")
			log.Print("Exception:" + err.Error())
			log.Print("Payload sent to the endpoint:")
			log.Print(string(dump))

			return 500, err
		}
	} else {
		//log and dump
		log.Printf("Content Type header not recognized, discarding request. Type: %s", contentType)
		return 400, nil
	}

	//Process the events
	controller.handler.ProcessEvent(eventType, string(jsonString))
	return 200, nil
}