package event_processors

import (
	"fmt"
	"github.com/google/go-github/v31/github"
	"github.com/tidwall/gjson"
	"gitlab.com/rice-patrick/github-bot/utils"
	"golang.org/x/net/context"
	"log"
)

//Processor for handling the pull request events
type PullRequestProcessor struct {

}

func (proc PullRequestProcessor) CheckEvent(eventType string) bool {
	if eventType == "pull_request"{
		return true
	} else {
		return false
	}
}

func (proc PullRequestProcessor) ProcessEvent(event string) {
	action := gjson.Get(event, "action")

	log.Printf("Event received for a pull request. Action: %s", action)
	if action.String() == "review_requested" {

		owner := gjson.Get(event, "repository.owner.login")
		repo := gjson.Get(event, "repository.name")
		issueNum := gjson.Get(event, "pull_request.number")
		logins := proc.identifyReviewers(event)

		proc.postResponse(owner.String(), repo.String(), issueNum.Int(), logins)

	} else {
		log.Printf("Action in event was not for a review, discarding event. Action: %s", action.String())
		return
	}


}

func (proc PullRequestProcessor) identifyReviewers(event string) []string {

	var logins []string = nil

	//If the plurality "reviewers" exists, then we need to process them all.
	if gjson.Get(event, "pull_request.requested_reviewers").Exists() {

		log.Printf("Multiple reviewers are present within the pull request, leaving a comment to multiple" +
			"reviewers.")
		var results []gjson.Result
		results = gjson.Get(event, "pull_request.requested_reviewers").Array()

		//Dont need the index, so we're going to dump it.
		for _, result := range results {
			username := result.Get("login")
			logins = append(logins, username.String())
		}

	// Otherwise check if the singleton exists, and leave a comment for just one. Much easier logic.
	} else if gjson.Get(event, "pull_request.requested_reviewer").Exists() {
		log.Printf("Single reviewer is present on the pull request. Leave single comment.")
		result := gjson.Get(event, "pull_request.requested_reviewers")
		logins = append(logins, result.Get("login").String())
	}

	return logins
}


func (proc PullRequestProcessor) postResponse(owner string, repo string, issueNum int64, reviewers []string) {

	list := ""
	for _, reviewer := range reviewers {
		list = list + fmt.Sprintf("@%s ", reviewer)
	}
	body := fmt.Sprintf("Review has been requested for all the below users \n %s", list)

	comment := github.IssueComment{Body: &body}

	iComment, _, err := utils.GetClient().Issues.CreateComment(context.Background(), owner, repo, int(issueNum), &comment)
	if err != nil {
		log.Printf("Error executing post to create a comment. Details:")
		log.Printf(err.Error())
	}

	if iComment != nil {
		log.Printf(fmt.Sprintf("URL For Comment: %s", iComment.URL))
	}
}
